// File: tedtalktest.c
//
// Description: test the tedtalk.c module
// @author      RIT CS Instructors
// @author      Martin Charles
//
// // // // // // // // // // // // // // // // // // // // // // // //

//
// Content supplied to the student.
//

#include <stdio.h>
#include <stdlib.h>

#include "EventTime.h"
#include "tedtalk.h"

void testTalk1() {
    printf("Creating a time...\n");
    EventTime_t *time = newEventTime(6, 30, "pm");

    printf("Starting tedtalk tests...\n");

    TEDtalk_t *talk = newTEDtalk("Do schools kill creativity?", "Ken Robinson");
    printf("First tedtalk initialized...\n");

    char *speaker = tedtalkGetSpeaker(talk);
    char *title = tedtalkGetTitle(talk);
    printf("The current tedtalk is: \n\t%s by %s.\n", title, speaker);
    free(speaker);
    free(title);

    char *talkStr = tedtalkToString(talk);
    printf("Otherwise... %s\n", talkStr);
    free(talkStr);

    tedtalkPlay(talk, time);

    char *afterPlybackStr = tedtalkToString(talk);
    printf("After playing the tedtalk... \n\t%s\n", afterPlybackStr);
    free(afterPlybackStr);

    EventTime_t *lastPlayedTime = tedtalkGetLastPlayed(talk);
    char *lastPlayedTimeStr = timeToString(lastPlayedTime);
    speaker = tedtalkGetSpeaker(talk);
    title = tedtalkGetTitle(talk);
    printf("The same tedtalk referencing members is: \n\t%s by %s. Last played "
           "at: %s\n",
           title, speaker, lastPlayedTimeStr);
    free(speaker);
    free(title);
    timeDelete(lastPlayedTime);
    free(lastPlayedTimeStr);

    tedtalkDelete(talk);
}

void testTalk2() {
    TEDtalk_t *talk = newTEDtalk("Your body shapes who you are", "Amy Cuddy");
    char *talkStr = tedtalkToString(talk);
    printf("The talk2 is: \n\t%s\n", talkStr);
    free(talkStr);

    TEDtalk_t *talkCopy = tedtalkCopy(talk);
    char *talkCopyStr = tedtalkToString(talkCopy);
    printf("The copy of talk2 tedtalk is: \n\t%s \n", talkCopyStr);
    tedtalkDelete(talk);
    free(talkCopyStr);

    char *talkCopyDeletedStr = tedtalkToString(talkCopy);
    printf("After deleting the original talk2, the copy of talk2 "
           "tedtalk is ... \n\t%s\n",
           talkCopyDeletedStr);

    TEDtalk_t *talkCopyCopy = tedtalkCopy(talkCopy);
    if (tedtalkEquals(talkCopy, talkCopyCopy)) {
        char *talkCopyCopyStr = tedtalkToString(talkCopyCopy);
        printf("The talk3 tedtalk \n\t%s\n"
               "    is the same as this tedtalk \n\t%s\n",
               talkCopyCopyStr, talkCopyDeletedStr);
        free(talkCopyCopyStr);
    } else {
        printf("ERROR: tedtalkCopy() failure!\n");
    }

    free(talkCopyDeletedStr);
    tedtalkDelete(talkCopyCopy);
    tedtalkDelete(talkCopy);
}

void testTalk3() {
    TEDtalk_t *talk =
        newTEDtalk("How great leaders inspire action", "Simon Sinek");
    char *talkStr = tedtalkToString(talk);
    printf("The current talk3 tedtalk is: \n\t%s\n", talkStr);
    free(talkStr);

    tedtalkPlay(talk, newEventTime(4, 42, "am"));

    char *talkPlayedStr = tedtalkToString(talk);
    printf("The current talk3 tedtalk is: \n\t%s\n", talkPlayedStr);
    free(talkPlayedStr);

    tedtalkDelete(talk);
}

/// main function tests the tedtalk module.
/// @returns errorCode  error Code; EXIT_SUCCESS if no error

int main(void) {
    testTalk1();
    testTalk2();
    testTalk3();

    return EXIT_SUCCESS;
}
